# Venados Test

Aplicación **Android** para la evaluación practica.

## Requisitos

- Dispositivo móvil con sistema operativo Android
	- Como alternativa se puede usar un dispositivo virtual a través de **Android Virtual Device** ([AVD](https://developer.android.com/studio/run/managing-avds?hl=es-419 "Android Virtual Device"))
- Versión mínima **SDK 21** ([Lollipop](https://developer.android.com/about/versions/android-5.0?hl=es-419 "Android Lollipop"))
- Versión objetivo **SDK 28** ([Pie](https://developer.android.com/about/versions/pie/android-9.0 "Android Pie"))
- Conexión a **internet** (Wi-Fi || Datos Móviles)

### Construir y Ejecutar

- Tener **Android Studio** instalado y configurado ([Instalación de Android Studio](https://developer.android.com/studio/install?hl=es-419 "Instalación de Android Studio"))
- Descargar el proyecto de este **repositorio** y alojarlo en un directorio dentro de su computadora.
- Iniciar **Android Studio** y abrir el proyecto desde la opción `Open an existing Android Studio project`

![enter image description here](http://i63.tinypic.com/osc28i.png)

- Luego **hacer** `Clean & Build`

![enter image description here](http://i64.tinypic.com/2diivzr.png)

- Después **ejecutarla** app desde `Run 'App'`

![enter image description here](http://i67.tinypic.com/dwqsxu.png)

- Seleccionar **dispositivo** físico o uno virtual

![enter image description here](http://i63.tinypic.com/se2253.png)

### Configurar dispositivo

- En el dispositivo físico habilitar las **opciones del desarrollador** para poder instalar aplicaciones desde Android Studio. Para activarlo es necesario presionar varias veces la versión de software.

![enter image description here](http://i68.tinypic.com/10i9h14.png)

- Luego ir a la **sección** `Opciones del Desarrollador` y activar `Depuración por USB`.

![enter image description here](http://i67.tinypic.com/ab2ss4.png)

![enter image description here](http://i65.tinypic.com/2romdyu.png)

- Al ejecutar la aplicación pedirá permiso para confiar en la computadora y posteriormente se instalará la aplicación en el dispositivo.

![enter image description here](http://i68.tinypic.com/2cgkdug.png)

