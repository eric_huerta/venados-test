package mx.sunwise.venadostest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.adapter.PlayersAdapter;
import mx.sunwise.venadostest.api.ApiAdapter;
import mx.sunwise.venadostest.api.IApiService;
import mx.sunwise.venadostest.api.response.players.implement.PlayersResponse;
import mx.sunwise.venadostest.listener.OnPlayerClickListener;
import mx.sunwise.venadostest.model.Player;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayersFragment extends Fragment {

    public static final String TAG = GamesFragment.class.getSimpleName();
    private static final int SPAN_COUNT = 3;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private PlayersAdapter adapter;

    public PlayersFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
        getPlayers();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), SPAN_COUNT));

        adapter = new PlayersAdapter(new ArrayList<Player>(), new OnPlayerClickListener() {
            @Override
            public void onItemClick(Player player) {
                showPlayerDetail(player);
            }
        });
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getPlayers();
                    }
                }
        );
    }

    private void updatePlayers(ArrayList<Player> players) {
        this.adapter.clear();
        this.adapter.addAll(players);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getPlayers() {
        IApiService service = ApiAdapter.getApiService();
        Call<PlayersResponse> call = service.getPlayers();

        call.enqueue(new Callback<PlayersResponse>() {
            @Override
            public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                ArrayList<Player> players = new ArrayList<>();
                players.addAll(response.body().getData().getTeam().getGoalkeepers());
                players.addAll(response.body().getData().getTeam().getDefenses());
                players.addAll(response.body().getData().getTeam().getCenters());
                players.addAll(response.body().getData().getTeam().getForwards());
                updatePlayers(players);
            }

            @Override
            public void onFailure(Call<PlayersResponse> call, Throwable t) {
                Log.e(TAG, "Something went wrong...Error message: " + t.getMessage());
            }
        });
    }

    private void showPlayerDetail(Player player) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag(PlayersDetailFragment.TAG);
        if (prev != null) fragmentTransaction.remove(prev);
        fragmentTransaction.addToBackStack(null);

        Bundle bundle = new Bundle();
        bundle.putSerializable(PlayersDetailFragment.KEY_PLAYER, player);

        DialogFragment dialogFragment = new PlayersDetailFragment();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fragmentTransaction, PlayersDetailFragment.TAG);
    }
}
