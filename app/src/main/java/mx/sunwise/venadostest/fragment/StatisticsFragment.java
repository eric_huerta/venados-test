package mx.sunwise.venadostest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.adapter.StatisticsAdapter;
import mx.sunwise.venadostest.api.ApiAdapter;
import mx.sunwise.venadostest.api.IApiService;
import mx.sunwise.venadostest.api.response.statistics.implement.StatisticsResponse;
import mx.sunwise.venadostest.model.Statistic;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticsFragment extends Fragment {

    public static final String TAG = GamesFragment.class.getSimpleName();

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private StatisticsAdapter adapter;

    public StatisticsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
        getStatistics();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new StatisticsAdapter(new ArrayList<Statistic>());
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getStatistics();
                    }
                }
        );
    }

    private void updateStatistics(ArrayList<Statistic> statistics) {
        this.adapter.clear();
        this.adapter.addAll(statistics);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getStatistics() {
        IApiService service = ApiAdapter.getApiService();
        Call<StatisticsResponse> call = service.getStatistics();

        call.enqueue(new Callback<StatisticsResponse>() {
            @Override
            public void onResponse(Call<StatisticsResponse> call, Response<StatisticsResponse> response) {
                ArrayList<Statistic> statistics = response.body().getData().getStatistics();
                updateStatistics(statistics);
            }

            @Override
            public void onFailure(Call<StatisticsResponse> call, Throwable t) {
                Log.e(TAG, "Something went wrong...Error message: " + t.getMessage());
            }
        });
    }
}
