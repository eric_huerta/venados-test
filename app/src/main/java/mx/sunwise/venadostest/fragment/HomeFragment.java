package mx.sunwise.venadostest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.api.ApiAdapter;
import mx.sunwise.venadostest.api.IApiService;
import mx.sunwise.venadostest.api.response.games.implement.GamesResponse;
import mx.sunwise.venadostest.listener.OnGamesListRefreshListener;
import mx.sunwise.venadostest.model.Game;
import mx.sunwise.venadostest.model.GameDate;
import mx.sunwise.venadostest.util.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnGamesListRefreshListener {

    public static final String TAG = HomeFragment.class.getSimpleName();
    public static final String COM = "COPA MX";
    public static final String LDA = "ASCENSO MX";

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    private GamesFragment gamesFragmentCOM;
    private GamesFragment gamesFragmentLDA;

    public HomeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setupTabLayout();
    }

    private void setupTabLayout() {
        setupViewPager(this.viewPager);
        this.tabLayout.setupWithViewPager(this.viewPager);
        this.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {}

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        this.onGamesListRefresh();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        gamesFragmentCOM = GamesFragment.newInstance(new ArrayList<Game>(), this);
        gamesFragmentLDA = GamesFragment.newInstance(new ArrayList<Game>(), this);

        viewPagerAdapter.addFragment(gamesFragmentCOM, COM);
        viewPagerAdapter.addFragment(gamesFragmentLDA, LDA);

        viewPager.setAdapter(viewPagerAdapter);
    }

    private void updateGamesOnFragments(ArrayList<Game> games) {
        ArrayList<Game> gamesCOM = new ArrayList<>();
        ArrayList<Game> gamesLDA = new ArrayList<>();

        String currentDateCOM = "";
        String currentDateLDA = "";
        String stringDate;

        for(Game game : games) {
            stringDate = DateUtils.getGameHeader(game.getDatetime());
            switch (game.getLeague().toUpperCase()) {
                case COM:
                    if (haveToAddHeader(currentDateCOM, stringDate)) {
                        currentDateCOM = stringDate;
                        gamesCOM.add(new GameDate(currentDateCOM));
                    }

                    gamesCOM.add(game);
                    break;

                case LDA:
                    if (haveToAddHeader(currentDateLDA, stringDate)) {
                        currentDateLDA = stringDate;
                        gamesLDA.add(new GameDate(currentDateLDA));
                    }

                    gamesLDA.add(game);
                    break;

                default:
                    break;
            }
        }

        gamesFragmentCOM.updateGames(gamesCOM);
        gamesFragmentLDA.updateGames(gamesLDA);
    }

    private boolean haveToAddHeader(String currentDate, String stringDate) {
        return (currentDate.isEmpty() || !currentDate.equals(stringDate));
    }

    @Override
    public void onGamesListRefresh() {
        IApiService service = ApiAdapter.getApiService();
        Call<GamesResponse> call = service.getGames();

        call.enqueue(new Callback<GamesResponse>() {
            @Override
            public void onResponse(Call<GamesResponse> call, Response<GamesResponse> response) {
                ArrayList<Game> games = response.body().getData().getGames();
                updateGamesOnFragments(games);
            }

            @Override
            public void onFailure(Call<GamesResponse> call, Throwable t) {
                Log.e(TAG, "Something went wrong...Error message: " + t.getMessage());
            }
        });
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        private void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }
    }

}
