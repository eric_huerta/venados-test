package mx.sunwise.venadostest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.model.Player;
import mx.sunwise.venadostest.util.CircleTransform;
import mx.sunwise.venadostest.util.DateUtils;

public class PlayersDetailFragment extends DialogFragment {

    public static final String TAG = PlayersDetailFragment.class.getSimpleName();
    public static final String KEY_PLAYER = "key_player";

    @BindView(R.id.fragment_players_detail_header_dismiss)
    ImageView dismiss;

    @BindView(R.id.fragment_players_detail_header_image)
    ImageView playerImage;

    @BindView(R.id.fragment_players_detail_header_name)
    TextView playerName;

    @BindView(R.id.fragment_players_detail_header_position)
    TextView playerPosition;

    @BindView(R.id.fragment_players_detail_header_number)
    TextView playerNumber;

    @BindView(R.id.fragment_players_detail_body_player_birthday)
    TextView playerBirthday;

    @BindView(R.id.fragment_players_detail_body_player_birth_place)
    TextView playerBirthPlace;

    @BindView(R.id.fragment_players_detail_body_player_weight)
    TextView playerWeight;

    @BindView(R.id.fragment_players_detail_body_player_height)
    TextView playerHeight;

    @BindView(R.id.fragment_players_detail_body_player_last_team)
    TextView playerLastTeam;

    private Player player;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert);

        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey(KEY_PLAYER))
            this.player = (Player) bundle.getSerializable(KEY_PLAYER);
        else
            dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_players_detail, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setData();
    }

    private void setData() {
        Picasso.get().load(player.getImage()).transform(new CircleTransform()).into(playerImage);

        String fullName = player.getName() + " " + player.getFirst_surname() + " " + player.getSecond_surname();
        playerName.setText(fullName);

        playerPosition.setText(player.getPosition());

        playerNumber.setText(String.format(Locale.getDefault(), "%d", player.getNumber()));

        String birthday = DateUtils.getDateFormated(player.getBirthday());
        playerBirthday.setText(birthday);

        playerBirthPlace.setText(player.getBirth_place());

        playerWeight.setText(String.format(Locale.getDefault(), "%d KG", player.getWeight()));

        playerHeight.setText(String.format(Locale.getDefault(), "%.2f M", player.getHeight()));

        playerLastTeam.setText(player.getLast_team());

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
