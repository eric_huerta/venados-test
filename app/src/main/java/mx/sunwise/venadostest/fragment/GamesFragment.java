package mx.sunwise.venadostest.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brandongogetap.stickyheaders.StickyLayoutManager;
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.adapter.GamesAdapter;
import mx.sunwise.venadostest.listener.OnGamesListRefreshListener;
import mx.sunwise.venadostest.model.Game;

/**
 * A simple {@link Fragment} subclass.
 */
public class GamesFragment extends Fragment {

    public static final String TAG = GamesFragment.class.getSimpleName();
    public static final String GAMES = "games";

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private GamesAdapter adapter;
    private ArrayList<Game> games;
    private OnGamesListRefreshListener onGamesListRefreshListener;

    public static GamesFragment newInstance(ArrayList<Game> games, Fragment fragment){
        GamesFragment gamesFragment = new GamesFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(GAMES, games);
        gamesFragment.setArguments(bundle);
        gamesFragment.onAttachToParentFragment(fragment);

        return gamesFragment;
    }

    public GamesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_games, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null)
            this.games = getArguments().getParcelableArrayList(GAMES);
        else
            this.games =  new ArrayList<>();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        adapter = new GamesAdapter(this.games);

        recyclerView.setLayoutManager(new StickyLayoutManager(getActivity(), adapter));
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        onGamesListRefreshListener.onGamesListRefresh();
                    }
                }
        );
    }

    public void updateGames(ArrayList<Game> games) {
        this.adapter.clear();
        this.adapter.addAll(games);
        swipeRefreshLayout.setRefreshing(false);
    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            onGamesListRefreshListener = (OnGamesListRefreshListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement OnPlayerSelectionSetListener");
        }
    }
}
