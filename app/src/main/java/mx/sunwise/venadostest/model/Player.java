package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Player extends Member implements Serializable {

    private static final String POSITION = "position";
    private static final String NUMBER = "number";
    private static final String POSITION_SHORT = "position_short";
    private static final String LAST_TEAM = "last_team";

    @SerializedName(POSITION)
    private String position;

    @SerializedName(NUMBER)
    private int number;

    @SerializedName(POSITION_SHORT)
    private String position_short;

    @SerializedName(LAST_TEAM)
    private String last_team;

    /**
     * GETTER & SETTER
     */

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPosition_short() {
        return position_short;
    }

    public void setPosition_short(String position_short) {
        this.position_short = position_short;
    }

    public String getLast_team() {
        return last_team;
    }

    public void setLast_team(String last_team) {
        this.last_team = last_team;
    }
}
