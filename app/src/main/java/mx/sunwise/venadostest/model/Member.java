package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public abstract class Member {

    private static final String NAME = "name";
    private static final String FIRST_SURNAME = "first_surname";
    private static final String SECOND_SURNAME = "second_surname";
    private static final String BIRTHDAY = "birthday";
    private static final String BIRTH_PLACE = "birth_place";
    private static final String WEIGHT = "weight";
    private static final String HEIGHT = "height";
    private static final String IMAGE = "image";

    @SerializedName(NAME)
    private String name;

    @SerializedName(FIRST_SURNAME)
    private String first_surname;

    @SerializedName(SECOND_SURNAME)
    private String second_surname;

    @SerializedName(BIRTHDAY)
    private Date birthday;

    @SerializedName(BIRTH_PLACE)
    private String birth_place;

    @SerializedName(WEIGHT)
    private int weight;

    @SerializedName(HEIGHT)
    private double height;

    @SerializedName(IMAGE)
    private String image;

    /**
     * GETTER & SETTER
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_surname() {
        return first_surname;
    }

    public void setFirst_surname(String first_surname) {
        this.first_surname = first_surname;
    }

    public String getSecond_surname() {
        return second_surname;
    }

    public void setSecond_surname(String second_surname) {
        this.second_surname = second_surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
