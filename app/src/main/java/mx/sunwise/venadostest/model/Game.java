package mx.sunwise.venadostest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Game implements Parcelable {

    private static final String LOCAL = "local";
    private static final String OPPONENT = "opponent";
    private static final String OPPONENT_IMAGE = "opponent_image";
    private static final String DATETIME = "datetime";
    private static final String LEAGUE = "league";
    private static final String IMAGE = "image";
    private static final String HOME_SCORE = "home_score";
    private static final String AWAY_SCORE = "away_score";

    @SerializedName(LOCAL)
    @Expose
    private boolean local;

    @SerializedName(OPPONENT)
    @Expose
    private String opponent;

    @SerializedName(OPPONENT_IMAGE)
    @Expose
    private String opponent_image;

    @SerializedName(DATETIME)
    @Expose
    private Date datetime;

    @SerializedName(LEAGUE)
    @Expose
    private String league;

    @SerializedName(IMAGE)
    @Expose
    private String image;

    @SerializedName(HOME_SCORE)
    @Expose
    private int home_score;

    @SerializedName(AWAY_SCORE)
    @Expose
    private int away_score;

    protected Game() {}

    /**
     * GETTER & SETTER
     */

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public String getOpponent_image() {
        return opponent_image;
    }

    public void setOpponent_image(String opponent_image) {
        this.opponent_image = opponent_image;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getHome_score() {
        return home_score;
    }

    public void setHome_score(int home_score) {
        this.home_score = home_score;
    }

    public int getAway_score() {
        return away_score;
    }

    public void setAway_score(int away_score) {
        this.away_score = away_score;
    }

    private Game(Parcel in) {
        local = in.readByte() != 0;
        opponent = in.readString();
        opponent_image = in.readString();
        league = in.readString();
        image = in.readString();
        home_score = in.readInt();
        away_score = in.readInt();
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (local ? 1 : 0));
        dest.writeString(opponent);
        dest.writeString(opponent_image);
        dest.writeString(league);
        dest.writeString(image);
        dest.writeInt(home_score);
        dest.writeInt(away_score);
    }
}
