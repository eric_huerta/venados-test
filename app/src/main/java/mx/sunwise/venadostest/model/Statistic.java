package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

public class Statistic {

    private static final String POSITION = "position";
    private static final String IMAGE = "image";
    private static final String TEAM = "team";
    private static final String GAMES = "games";
    private static final String WIN = "win";
    private static final String LOSS = "loss";
    private static final String TIE = "tie";
    private static final String F_GOALS = "f_goals";
    private static final String A_GOALS = "a_goals";
    private static final String SCORE_DIFF = "score_diff";
    private static final String POINTS = "points";
    private static final String EFEC = "efec";

    @SerializedName(POSITION)
    private int position;

    @SerializedName(IMAGE)
    private String image;

    @SerializedName(TEAM)
    private String team;

    @SerializedName(GAMES)
    private int games;

    @SerializedName(WIN)
    private int win;

    @SerializedName(LOSS)
    private int loss;

    @SerializedName(TIE)
    private int tie;

    @SerializedName(F_GOALS)
    private int f_goals;

    @SerializedName(A_GOALS)
    private int a_goals;

    @SerializedName(SCORE_DIFF)
    private int score_diff;

    @SerializedName(POINTS)
    private int points;

    @SerializedName(EFEC)
    private int efec;

    /**
     * GETTER & SETTER
     */

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public int getTie() {
        return tie;
    }

    public void setTie(int tie) {
        this.tie = tie;
    }

    public int getF_goals() {
        return f_goals;
    }

    public void setF_goals(int f_goals) {
        this.f_goals = f_goals;
    }

    public int getA_goals() {
        return a_goals;
    }

    public void setA_goals(int a_goals) {
        this.a_goals = a_goals;
    }

    public int getScore_diff() {
        return score_diff;
    }

    public void setScore_diff(int score_diff) {
        this.score_diff = score_diff;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getEfec() {
        return efec;
    }

    public void setEfec(int efec) {
        this.efec = efec;
    }
}
