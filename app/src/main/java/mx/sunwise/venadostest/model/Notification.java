package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Notification {

    private static final String TIMESTAMP = "timestamp";
    private static final String MESSAGE = "message";

    @SerializedName(TIMESTAMP)
    private Date timestamp;

    @SerializedName(MESSAGE)
    private String message;

    /**
     * GETTER & SETTER
     */

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
