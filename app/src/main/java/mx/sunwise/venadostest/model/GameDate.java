package mx.sunwise.venadostest.model;

import android.os.Parcelable;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameDate extends Game implements StickyHeader {

    private static final String STRING_DATE = "string_date";

    @SerializedName(STRING_DATE)
    @Expose
    private String stringDate;

    public GameDate(String stringDate) {
        this.stringDate = stringDate;
    }

    /**
     * GETTER & SETTER
     */

    public String getStringDate() {
        return stringDate;
    }

    public void setStringDate(String stringDate) {
        this.stringDate = stringDate;
    }
}
