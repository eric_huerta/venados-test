package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

public class Coach extends Member {

    private static final String ROLE = "role";
    private static final String ROLE_SHORT = "role_short";

    @SerializedName(ROLE)
    private String role;

    @SerializedName(ROLE_SHORT)
    private String role_short;

    /**
     * GETTER & SETTER
     */

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_short() {
        return role_short;
    }

    public void setRole_short(String role_short) {
        this.role_short = role_short;
    }
}
