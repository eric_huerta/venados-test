package mx.sunwise.venadostest.model;

import com.google.gson.annotations.SerializedName;

public class Sponsor {

    private static final String NAME = "name";
    private static final String URL = "url";
    private static final String PHONE = "phone";
    private static final String ADDRESS = "address";
    private static final String IMAGE = "image";
    private static final String FACEBOOK = "facebook";
    private static final String TWITTER = "twitter";
    private static final String INSTAGRAM = "instagram";

    @SerializedName(NAME)
    private String name;

    @SerializedName(URL)
    private String url;

    @SerializedName(PHONE)
    private String phone;

    @SerializedName(ADDRESS)
    private String address;

    @SerializedName(IMAGE)
    private String image;

    @SerializedName(FACEBOOK)
    private String facebook;

    @SerializedName(TWITTER)
    private String twitter;

    @SerializedName(INSTAGRAM)
    private String instagram;

    /**
     * GETTER & SETTER
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
}
