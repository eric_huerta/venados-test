package mx.sunwise.venadostest.util;

public class Constants {

    public static final class IMAGE {
        public static final int TARGET_WIDTH_64 = 64;
        public static final int TARGET_HEIGH_64 = 64;
        public static final String LOGO_URL = "https://venados.dacodes.mx/img/venados.png";
    }

}
