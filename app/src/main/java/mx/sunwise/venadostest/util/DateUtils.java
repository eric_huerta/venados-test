package mx.sunwise.venadostest.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static final String[] NAMES_OF_DAYS_OF_WEEK = new String[]{"SUN", "MON", "TUE", "WED", "THU", "FIR", "SAT"};
    public static final String[] NAMES_OF_MONTHS = new String[]{"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};
    public static final String DD_MM_YYYY = "dd/MM/yyyy";

    public static String getNameOfDayOfWeek(int day) {
        return (DateUtils.NAMES_OF_DAYS_OF_WEEK.length >= day) ? DateUtils.NAMES_OF_DAYS_OF_WEEK[day - 1] : "N/A";
    }

    public static String getNameOfMonth(int month) {
        return (DateUtils.NAMES_OF_MONTHS.length >= month) ? DateUtils.NAMES_OF_MONTHS[month] : "N/A";
    }

    public static String getGameHeader(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        int year  = calendar.get(Calendar.YEAR);

        String stringMonth = DateUtils.getNameOfMonth(month);
        return String.format(Locale.getDefault(), "%s %d", stringMonth, year);
    }

    public static String getDateFormated(Date date) {
        return DateUtils.getDateFormated(date, DateUtils.DD_MM_YYYY);
    }

    public static String getDateFormated(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return simpleDateFormat.format(date);
    }
}
