package mx.sunwise.venadostest.listener;

import mx.sunwise.venadostest.model.Player;

public interface OnPlayerClickListener {
    void onItemClick(Player player);
}
