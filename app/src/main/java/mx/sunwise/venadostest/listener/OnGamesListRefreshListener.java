package mx.sunwise.venadostest.listener;

public interface OnGamesListRefreshListener {
    void onGamesListRefresh();
}
