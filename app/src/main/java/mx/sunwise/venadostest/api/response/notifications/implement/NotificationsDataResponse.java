package mx.sunwise.venadostest.api.response.notifications.implement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.notifications.interfaces.INotificationsDataResponse;
import mx.sunwise.venadostest.model.Notification;

public class NotificationsDataResponse implements INotificationsDataResponse {

    @SerializedName(NOTIFICATIONS)
    private ArrayList<Notification> notifications;

    @SerializedName(CODE)
    private int code;

    @Override
    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    @Override
    public void setNotifications(ArrayList<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

}
