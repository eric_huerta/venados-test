package mx.sunwise.venadostest.api.response.base;

public interface IBaseResponse {

    String SUCCESS = "success";
    String DATA = "data";

    boolean isSuccess();

    void setSuccess(boolean success);

}
