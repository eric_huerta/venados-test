package mx.sunwise.venadostest.api.response.games.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.games.interfaces.IGamesResponse;

public class GamesResponse implements IGamesResponse {

    @SerializedName(SUCCESS)
    private boolean success;

    @SerializedName(DATA)
    private GamesDataResponse data;

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public GamesDataResponse getData() {
        return data;
    }

    @Override
    public void setData(GamesDataResponse data) {
        this.data = data;
    }

}
