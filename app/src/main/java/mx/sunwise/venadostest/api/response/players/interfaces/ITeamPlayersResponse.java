package mx.sunwise.venadostest.api.response.players.interfaces;

import java.util.ArrayList;

import mx.sunwise.venadostest.model.Coach;
import mx.sunwise.venadostest.model.Player;

public interface ITeamPlayersResponse {

    String FORWARDS = "forwards";
    String CENTERS = "centers";
    String DEFENSES = "defenses";
    String GOALKEEPERS = "goalkeepers";
    String COACHES = "coaches";

    ArrayList<Player> getForwards();

    void setForwards(ArrayList<Player> forwards);

    ArrayList<Player> getCenters();

    void setCenters(ArrayList<Player> centers);

    ArrayList<Player> getDefenses();

    void setDefenses(ArrayList<Player> defenses);

    ArrayList<Player> getGoalkeepers();

    void setGoalkeepers(ArrayList<Player> goalkeepers);

    ArrayList<Coach> getCoaches();

    void setCoaches(ArrayList<Coach> coaches);

}
