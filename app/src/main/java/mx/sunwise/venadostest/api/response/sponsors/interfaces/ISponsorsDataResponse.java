package mx.sunwise.venadostest.api.response.sponsors.interfaces;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.base.IBaseDataResponse;
import mx.sunwise.venadostest.model.Sponsor;

public interface ISponsorsDataResponse extends IBaseDataResponse {

    ArrayList<Sponsor> getSponsors();

    void setSponsors(ArrayList<Sponsor> sponsors);

}
