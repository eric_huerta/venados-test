package mx.sunwise.venadostest.api.response.games.implement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.games.interfaces.IGamesDataResponse;
import mx.sunwise.venadostest.model.Game;

public class GamesDataResponse implements IGamesDataResponse{

    @SerializedName(GAMES)
    private ArrayList<Game> games;

    @SerializedName(CODE)
    private int code;

    @Override
    public ArrayList<Game> getGames() {
        return games;
    }

    @Override
    public void setGames(ArrayList<Game> games) {
        this.games = games;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

}
