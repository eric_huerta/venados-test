package mx.sunwise.venadostest.api.response.notifications.interfaces;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.base.IBaseDataResponse;
import mx.sunwise.venadostest.model.Notification;

public interface INotificationsDataResponse extends IBaseDataResponse {

    ArrayList<Notification> getNotifications();

    void setNotifications(ArrayList<Notification> notifications);

}
