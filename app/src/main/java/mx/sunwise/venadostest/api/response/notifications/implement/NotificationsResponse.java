package mx.sunwise.venadostest.api.response.notifications.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.notifications.interfaces.INotificationsResponse;

public class NotificationsResponse implements INotificationsResponse {

    @SerializedName(SUCCESS)
    private boolean success;

    @SerializedName(DATA)
    private NotificationsDataResponse data;

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public NotificationsDataResponse getData() {
        return data;
    }

    @Override
    public void setData(NotificationsDataResponse data) {
        this.data = data;
    }

}
