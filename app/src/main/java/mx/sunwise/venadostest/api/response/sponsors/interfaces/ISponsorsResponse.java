package mx.sunwise.venadostest.api.response.sponsors.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseResponse;
import mx.sunwise.venadostest.api.response.sponsors.implement.SponsorsDataResponse;

public interface ISponsorsResponse extends IBaseResponse {

    SponsorsDataResponse getData();

    void setData(SponsorsDataResponse data);

}
