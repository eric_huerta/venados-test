package mx.sunwise.venadostest.api.response.statistics.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.statistics.interfaces.IStatisticsResponse;

public class StatisticsResponse implements IStatisticsResponse{

    @SerializedName(SUCCESS)
    private boolean success;

    @SerializedName(DATA)
    private StatisticsDataResponse data;

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public StatisticsDataResponse getData() {
        return data;
    }

    @Override
    public void setData(StatisticsDataResponse data) {
        this.data = data;
    }

}
