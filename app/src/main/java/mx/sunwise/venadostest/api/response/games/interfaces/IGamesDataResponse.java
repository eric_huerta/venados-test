package mx.sunwise.venadostest.api.response.games.interfaces;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.base.IBaseDataResponse;
import mx.sunwise.venadostest.model.Game;

public interface IGamesDataResponse extends IBaseDataResponse {

    ArrayList<Game> getGames();

    void setGames(ArrayList<Game> games);

}
