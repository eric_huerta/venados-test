package mx.sunwise.venadostest.api.response.players.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.players.interfaces.IPlayersDataResponse;

public class PlayersDataResponse implements IPlayersDataResponse {

    @SerializedName(TEAM)
    private TeamPlayersResponse team;

    @SerializedName(CODE)
    private int code;

    @Override
    public TeamPlayersResponse getTeam() {
        return team;
    }

    @Override
    public void setTeam(TeamPlayersResponse team) {
        this.team = team;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

}
