package mx.sunwise.venadostest.api.response.sponsors.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.sponsors.interfaces.ISponsorsResponse;

public class SponsorsResponse implements ISponsorsResponse {

    @SerializedName(SUCCESS)
    private boolean success;

    @SerializedName(DATA)
    private SponsorsDataResponse data;

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public SponsorsDataResponse getData() {
        return data;
    }

    @Override
    public void setData(SponsorsDataResponse data) {
        this.data = data;
    }

}
