package mx.sunwise.venadostest.api.response.statistics.interfaces;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.base.IBaseDataResponse;
import mx.sunwise.venadostest.model.Statistic;

public interface IStatisticsDataResponse extends IBaseDataResponse {

    ArrayList<Statistic> getStatistics();

    void setStatistics(ArrayList<Statistic> statistics);

}
