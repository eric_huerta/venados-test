package mx.sunwise.venadostest.api.response.statistics.implement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.statistics.interfaces.IStatisticsDataResponse;
import mx.sunwise.venadostest.model.Statistic;

public class StatisticsDataResponse implements IStatisticsDataResponse {

    @SerializedName(STATISTICS)
    private ArrayList<Statistic> statistics;

    @SerializedName(CODE)
    private int code;

    @Override
    public ArrayList<Statistic> getStatistics() {
        return statistics;
    }

    @Override
    public void setStatistics(ArrayList<Statistic> statistics) {
        this.statistics = statistics;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

}
