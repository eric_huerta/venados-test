package mx.sunwise.venadostest.api.response.sponsors.implement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.sponsors.interfaces.ISponsorsDataResponse;
import mx.sunwise.venadostest.model.Sponsor;

public class SponsorsDataResponse implements ISponsorsDataResponse {

    @SerializedName(SPONSORS)
    private ArrayList<Sponsor> sponsors;

    @SerializedName(CODE)
    private int code;

    @Override
    public ArrayList<Sponsor> getSponsors() {
        return sponsors;
    }

    @Override
    public void setSponsors(ArrayList<Sponsor> sponsors) {
        this.sponsors = sponsors;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

}
