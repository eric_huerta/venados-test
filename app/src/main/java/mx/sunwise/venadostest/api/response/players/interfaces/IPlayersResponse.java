package mx.sunwise.venadostest.api.response.players.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseResponse;
import mx.sunwise.venadostest.api.response.players.implement.PlayersDataResponse;

public interface IPlayersResponse extends IBaseResponse {

    PlayersDataResponse getData();

    void setData(PlayersDataResponse data);

}
