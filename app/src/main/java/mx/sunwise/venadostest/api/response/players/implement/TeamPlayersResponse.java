package mx.sunwise.venadostest.api.response.players.implement;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import mx.sunwise.venadostest.api.response.players.interfaces.ITeamPlayersResponse;
import mx.sunwise.venadostest.model.Coach;
import mx.sunwise.venadostest.model.Player;

public class TeamPlayersResponse implements ITeamPlayersResponse {

    @SerializedName(FORWARDS)
    private ArrayList<Player> forwards;

    @SerializedName(CENTERS)
    private ArrayList<Player> centers;

    @SerializedName(DEFENSES)
    private ArrayList<Player> defenses;

    @SerializedName(GOALKEEPERS)
    private ArrayList<Player> goalkeepers;

    @SerializedName(COACHES)
    private ArrayList<Coach> coaches;

    @Override
    public ArrayList<Player> getForwards() {
        return forwards;
    }

    @Override
    public void setForwards(ArrayList<Player> forwards) {
        this.forwards = forwards;
    }

    @Override
    public ArrayList<Player> getCenters() {
        return centers;
    }

    @Override
    public void setCenters(ArrayList<Player> centers) {
        this.centers = centers;
    }

    @Override
    public ArrayList<Player> getDefenses() {
        return defenses;
    }

    @Override
    public void setDefenses(ArrayList<Player> defenses) {
        this.defenses = defenses;
    }

    @Override
    public ArrayList<Player> getGoalkeepers() {
        return goalkeepers;
    }

    @Override
    public void setGoalkeepers(ArrayList<Player> goalkeepers) {
        this.goalkeepers = goalkeepers;
    }

    @Override
    public ArrayList<Coach> getCoaches() {
        return coaches;
    }

    @Override
    public void setCoaches(ArrayList<Coach> coaches) {
        this.coaches = coaches;
    }
}
