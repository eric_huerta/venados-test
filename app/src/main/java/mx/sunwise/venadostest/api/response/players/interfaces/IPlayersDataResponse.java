package mx.sunwise.venadostest.api.response.players.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseDataResponse;
import mx.sunwise.venadostest.api.response.players.implement.TeamPlayersResponse;

public interface IPlayersDataResponse extends IBaseDataResponse {

    TeamPlayersResponse getTeam();

    void setTeam(TeamPlayersResponse team);

}
