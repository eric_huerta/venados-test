package mx.sunwise.venadostest.api.response.statistics.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseResponse;
import mx.sunwise.venadostest.api.response.statistics.implement.StatisticsDataResponse;

public interface IStatisticsResponse extends IBaseResponse {

    StatisticsDataResponse getData();

    void setData(StatisticsDataResponse data);

}
