package mx.sunwise.venadostest.api.response.base;

public interface IBaseDataResponse {

    String CODE = "code";
    String GAMES = "games";
    String NOTIFICATIONS = "notifications";
    String TEAM = "team";
    String SPONSORS = "sponsors";
    String STATISTICS = "statistics";

    int getCode();

    void setCode(int code);

}
