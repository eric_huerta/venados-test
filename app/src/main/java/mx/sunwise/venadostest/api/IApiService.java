package mx.sunwise.venadostest.api;

import mx.sunwise.venadostest.api.response.games.implement.GamesResponse;
import mx.sunwise.venadostest.api.response.notifications.implement.NotificationsResponse;
import mx.sunwise.venadostest.api.response.players.implement.PlayersResponse;
import mx.sunwise.venadostest.api.response.sponsors.implement.SponsorsResponse;
import mx.sunwise.venadostest.api.response.statistics.implement.StatisticsResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface IApiService {

    @GET("statistics")
    Call<StatisticsResponse> getStatistics();

    @GET("games")
    Call<GamesResponse> getGames();

    @GET("players")
    Call<PlayersResponse> getPlayers();

    @GET("sponsors")
    Call<SponsorsResponse> getSponsors();

    @GET("notifications")
    Call<NotificationsResponse> getNotifications();

}
