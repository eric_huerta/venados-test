package mx.sunwise.venadostest.api.response.games.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseResponse;
import mx.sunwise.venadostest.api.response.games.implement.GamesDataResponse;

public interface IGamesResponse extends IBaseResponse {

    GamesDataResponse getData();

    void setData(GamesDataResponse data);

}
