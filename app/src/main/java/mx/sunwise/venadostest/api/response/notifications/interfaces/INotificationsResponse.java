package mx.sunwise.venadostest.api.response.notifications.interfaces;

import mx.sunwise.venadostest.api.response.base.IBaseResponse;
import mx.sunwise.venadostest.api.response.notifications.implement.NotificationsDataResponse;

public interface INotificationsResponse extends IBaseResponse {

    NotificationsDataResponse getData();

    void setData(NotificationsDataResponse data);

}
