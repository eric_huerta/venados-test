package mx.sunwise.venadostest.api.response.players.implement;

import com.google.gson.annotations.SerializedName;

import mx.sunwise.venadostest.api.response.players.interfaces.IPlayersResponse;

public class PlayersResponse implements IPlayersResponse {

    @SerializedName(SUCCESS)
    private boolean success;

    @SerializedName(DATA)
    private PlayersDataResponse data;

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public PlayersDataResponse getData() {
        return data;
    }

    @Override
    public void setData(PlayersDataResponse data) {
        this.data = data;
    }

}
