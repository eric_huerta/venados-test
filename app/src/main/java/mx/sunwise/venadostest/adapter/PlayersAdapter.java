package mx.sunwise.venadostest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.listener.OnPlayerClickListener;
import mx.sunwise.venadostest.model.Player;
import mx.sunwise.venadostest.util.CircleTransform;

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.PlayersViewHolder> {

    private List<Player> players;
    private final OnPlayerClickListener listener;

    public PlayersAdapter(List<Player> games, OnPlayerClickListener listener) {
        this.players = games;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PlayersAdapter.PlayersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_players_item, viewGroup, false);
        return new PlayersAdapter.PlayersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayersAdapter.PlayersViewHolder gameViewHolder, int i) {
        gameViewHolder.bind(players.get(i), listener);
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public void addAll(List<Player> players){
        this.players.addAll(players);
        notifyDataSetChanged();
    }

    public void clear(){
        this.players.clear();
        notifyDataSetChanged();
    }

    public static class PlayersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fragment_players_item_image)
        ImageView playerImage;

        @BindView(R.id.fragment_players_item_name)
        TextView playerPosition;

        @BindView(R.id.fragment_players_item_position)
        TextView playerName;

        public Context context;

        private PlayersViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        private void bind(@NonNull final Player player, final OnPlayerClickListener listener) {
            Picasso.get().load(player.getImage()).transform(new CircleTransform()).into(playerImage);
            playerName.setText(player.getPosition());

            String fullName = player.getName() + " " + player.getFirst_surname() + " " + player.getSecond_surname();
            playerPosition.setText(fullName);

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(player);
                }
            });
        }
    }

}
