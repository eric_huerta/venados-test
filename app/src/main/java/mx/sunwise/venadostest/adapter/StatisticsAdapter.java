package mx.sunwise.venadostest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.model.Statistic;
import mx.sunwise.venadostest.util.Constants;

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.StatisticsViewHolder> {

    private List<Statistic> games;

    public StatisticsAdapter(List<Statistic> games) {
        this.games = games;
    }

    @NonNull
    @Override
    public StatisticsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_statistics_item, viewGroup, false);
        return new StatisticsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsViewHolder gameViewHolder, int i) {
        gameViewHolder.bind(games.get(i));
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public void addAll(List<Statistic> statistics){
        this.games.addAll(statistics);
        notifyDataSetChanged();
    }

    public void clear(){
        this.games.clear();
        notifyDataSetChanged();
    }

    public static class StatisticsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fragment_statistics_item_position)
        TextView position;

        @BindView(R.id.fragment_statistics_item_team_image)
        ImageView teamImage;

        @BindView(R.id.fragment_statistics_item_team_name)
        TextView teamName;

        @BindView(R.id.fragment_statistics_item_team_jj)
        TextView teamJJ;

        @BindView(R.id.fragment_statistics_item_team_dg)
        TextView teamDG;

        @BindView(R.id.fragment_statistics_item_team_pts)
        TextView teamPTS;

        public Context context;

        private StatisticsViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        private void bind(@NonNull Statistic statistic) {
            position.setText(String.format(Locale.getDefault(), "%d", statistic.getPosition()));
            Picasso.get().load(statistic.getImage()).resize(Constants.IMAGE.TARGET_WIDTH_64, Constants.IMAGE.TARGET_HEIGH_64).into(teamImage);
            teamName.setText(statistic.getTeam());
            teamJJ.setText(String.format(Locale.getDefault(),  "%d", statistic.getGames()));
            teamDG.setText(String.format(Locale.getDefault(),  "%d", statistic.getScore_diff()));
            teamPTS.setText(String.format(Locale.getDefault(), "%d", statistic.getPoints()));
        }
    }

}
