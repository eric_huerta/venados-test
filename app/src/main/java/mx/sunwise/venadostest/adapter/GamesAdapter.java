package mx.sunwise.venadostest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.sunwise.venadostest.R;
import mx.sunwise.venadostest.model.Game;
import mx.sunwise.venadostest.model.GameDate;
import mx.sunwise.venadostest.util.Constants;
import mx.sunwise.venadostest.util.DateUtils;

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GameViewHolder> implements StickyHeaderHandler {

    private List<Game> games;

    public GamesAdapter(List<Game> games) {
        this.games = games;
    }

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_games_item, viewGroup, false);
        return new GameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GameViewHolder gameViewHolder, int i) {
        Game game = games.get(i);
        if (game instanceof StickyHeader) {
            gameViewHolder.bind((GameDate) game);
        } else {
            gameViewHolder.bind(game);
        }
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public void addAll(List<Game> games){
        this.games.addAll(games);
        notifyDataSetChanged();
    }

    public void clear(){
        this.games.clear();
        notifyDataSetChanged();
    }

    @Override
    public List<?> getAdapterData() {
        return games;
    }

    public static class GameViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.games_fragment_header_container)
        View headerContainer;

        @BindView(R.id.games_fragment_header_label)
        TextView dateString;

        @BindView(R.id.games_fragment_item_container)
        View itemContainer;

        @BindView(R.id.fragment_games_item_calendar_layout)
        View calendarLayout;

        @BindView(R.id.fragment_games_item_calendar_day_of_month)
        TextView dayOfMonth;

        @BindView(R.id.fragment_games_item_calendar_day_of_week)
        TextView dayOfWeek;

        @BindView(R.id.fragment_games_item_info_home_team_image)
        ImageView homeTeamImage;

        @BindView(R.id.fragment_games_item_info_home_team_name)
        TextView homeTeamName;

        @BindView(R.id.fragment_games_item_info_home_team_score)
        TextView homeTeamScore;

        @BindView(R.id.fragment_games_item_info_away_team_image)
        ImageView awayTeamImage;

        @BindView(R.id.fragment_games_item_info_away_team_name)
        TextView awayTeamName;

        @BindView(R.id.fragment_games_item_info_away_team_score)
        TextView awayTeamScore;

        public Context context;

        private GameViewHolder(View view) {
            super(view);
            context = view.getContext();
            ButterKnife.bind(this, view);
        }

        private void bind(@NonNull Game game) {
            headerContainer.setVisibility(View.GONE);
            itemContainer.setVisibility(View.VISIBLE);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(game.getDatetime());
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            this.dayOfMonth.setText(String.format(Locale.getDefault(), "%d", dayOfMonth));
            this.dayOfWeek.setText(DateUtils.getNameOfDayOfWeek(dayOfWeek));

            Picasso.get().load(game.isLocal() ? game.getOpponent_image() : Constants.IMAGE.LOGO_URL).
                    resize(Constants.IMAGE.TARGET_WIDTH_64, Constants.IMAGE.TARGET_HEIGH_64).into(awayTeamImage);
            Picasso.get().load(game.isLocal() ? Constants.IMAGE.LOGO_URL : game.getOpponent_image()).
                    resize(Constants.IMAGE.TARGET_WIDTH_64, Constants.IMAGE.TARGET_HEIGH_64).into(homeTeamImage);

            awayTeamName.setText(game.isLocal() ? game.getOpponent() : context.getResources().getString(R.string.app_team_name));
            homeTeamName.setText(game.isLocal() ? context.getResources().getString(R.string.app_team_name) : game.getOpponent());

            homeTeamScore.setText(String.format(Locale.getDefault(), "%d", game.getHome_score()));
            awayTeamScore.setText(String.format(Locale.getDefault(), "%d", game.getAway_score()));
        }

        private void bind(@NonNull GameDate gameDate) {
            itemContainer.setVisibility(View.GONE);
            headerContainer.setVisibility(View.VISIBLE);
            dateString.setText(gameDate.getStringDate());
        }
    }

}
